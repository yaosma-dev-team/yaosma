package com.vgsl.yaosma.audio;

import com.vgsl.yaosma.model.SoundButton;
import com.vgsl.yaosma.model.SoundTrack;
import com.vgsl.yaosma.model.TimedSound;

/**
 * Created by Sykefu on 02/04/2015.
 */
public class SoundTrackLoader {

    private final int SOUNDTRACK_ARRAY_LIST_SIZE = 50;
    public static final long MILLION = 1000000;

    private SoundTrack track;
    private boolean recording;
    private long recordingStartTime;

    public SoundTrackLoader(){
        this.track = null;
        recordingStartTime = 0;
        recording = false;
    }


    public boolean isRecording() {
        return recording;
    }

    /**
     * Start the recording of played sound, allowing to save them in the soundtrack
     */
    public void buildTrack(Player _player) {
        track = new SoundTrack(SOUNDTRACK_ARRAY_LIST_SIZE, _player);
        recordingStartTime = System.nanoTime()/MILLION;
        recording = true;
    }

    /**
     * Stops the recording of played sounds, also change the time of the
     * first sound played to zero millisecond to make it start when the track is started
     */

    public SoundTrack sendTrack() {
        if(track.getSoundTrack().size() != 0) {
            TimedSound st;

            recording = false;
            track.setEndTime(System.nanoTime() / MILLION - recordingStartTime);
            long offset;
            st = (TimedSound) track.getSoundTrack().get(0);
            offset = st.getTime();
            for (int i = 0; i < track.getSoundTrack().size(); i++) {
                st = (TimedSound) track.getSoundTrack().get(i);
                st.setTime(st.getTime() - offset);
            }
            track.setEndTime(track.getEndTime() - offset);
        }
        return track;
    }

    /**
     * Function used to add a sound to the soundtrack when a soundbutton is pressed
     * @param b the pressed soundbutton
     */
    public void addPressed(SoundButton b) {
        track.getSoundTrack().add(new TimedSound(b.getSound(), System.nanoTime() / MILLION - recordingStartTime));
    }

    public void addReleased(SoundButton button) {
        // TODO fix HOLD sound in soundtrack
    }
}

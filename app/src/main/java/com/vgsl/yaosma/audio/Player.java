package com.vgsl.yaosma.audio;

import android.media.AudioManager;
import android.media.SoundPool;

import com.vgsl.yaosma.model.PlayStyle;
import com.vgsl.yaosma.model.Sound;

/**
 * Created by Giyomm on 12/02/2015.
 */
public class Player {

    public SoundPool pool;

    public Player() {
        final int MAX_CONCURRENT_SOUNDS = 10;
        pool = new SoundPool(MAX_CONCURRENT_SOUNDS, AudioManager.STREAM_MUSIC, 0);
    }

    public void playSound(Sound sound) {
        //TODO use system volume
        float volume = 1.0f;

        PlayStyle playStyle = sound.getPlayStyle();
        if(playStyle == PlayStyle.PUNCTUAL) {
            pool.play(sound.getSoundId(), volume, volume, 0, 0, 1f); // TODO remove redundancy

        }
        else if(playStyle == PlayStyle.LOOP) {
            if(sound.isBeingPlayed()) {
                pool.stop(sound.getStreamId());
                sound.setStreamId(-1);
            } else {
                playInLoop(sound, volume);
            }

        } else if(playStyle == PlayStyle.HOLD) {
            int streamId = pool.play(sound.getSoundId(), volume, volume, 0, 0, 1f);
            sound.setStreamId(streamId);
        }
    }

    private void playInLoop(Sound sound, float volume) {
        int loop = -1;
        int streamId = pool.play(sound.getSoundId(), volume, volume, 0, loop, 1f);
        sound.setStreamId(streamId);
    }
}

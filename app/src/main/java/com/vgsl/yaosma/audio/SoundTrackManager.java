package com.vgsl.yaosma.audio;

import com.vgsl.yaosma.model.SoundTrack;

/**
 * Created by sykefu on 26/03/15.
 */
public class SoundTrackManager {
    private final int TRACK_SIZE = 5;
    private SoundTrack[] tracks;
    private int index;

    public SoundTrackManager(){
        tracks = new SoundTrack[TRACK_SIZE];
        index = 0;
    }

    public SoundTrack getTrack(int _trackNumber){
        return tracks[_trackNumber];
    }

    public int getIndex(){return index;}

    public int getTRACK_SIZE() {
        return TRACK_SIZE;
    }

    public SoundTrack getLastTrack(){
        return tracks[index];
    }

    /**
     * Call this when building a new soundtrack, will allow the soundtrack to be built and record the buttons pressed
     */
    public void receiveTrack(SoundTrack _track) {
        // TODO check if we can merge receiveTrack and incrementIndex into one method
        tracks[index] = _track;
    }


    /**
     * Removes the track at index _toRemove
     * @param _toRemove index of the track to remove
     */
    public void removeTrack(int _toRemove){
        tracks[_toRemove] = null;
        if(_toRemove < TRACK_SIZE - 1){
            for(int i = _toRemove; i < TRACK_SIZE - 1; i++){
                tracks[i] = tracks[i+1];
            }
        }
        index--;
    }

    /**
     * Used to prevent outofbound allocations
     * @return true if you can still add soundtracks
     */
    public void incrementIndex() {
            index++;
    }

    public boolean sizeIsValid(){
        return index < TRACK_SIZE;
    }
}

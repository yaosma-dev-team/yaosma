package com.vgsl.yaosma.audio;

import com.vgsl.yaosma.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 12/03/15.
 */
public final class SoundMapper {

    public static List<String> extract(Field[] sounds) {
        List<String> soundNames = new ArrayList<>(sounds.length);
        for(Field f: sounds) {
            soundNames.add(f.getName());
        }
        return soundNames;
    }


    public static int getResIdFromName(String name) {
        Field[] sounds = R.raw.class.getFields();
        for(Field sound : sounds) {
            if(sound.getName().equals(name))  {
                try {
                    return sound.getInt(null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }

}

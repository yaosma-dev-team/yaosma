package com.vgsl.yaosma.audio;

import com.vgsl.yaosma.model.SoundTrack;
import com.vgsl.yaosma.model.TimedSound;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sykefu on 02/04/2015.
 */
public class SoundTrackPlayer {

    private boolean[] playing;
    private Timer[] clock;
    private int[] trackIndex;
    private SoundTrackManager trackManager;

    public SoundTrackPlayer(SoundTrackManager _trackManager){
        this.trackManager = _trackManager;
        playing = new boolean[this.trackManager.getTRACK_SIZE()];
        clock = new Timer[this.trackManager.getTRACK_SIZE()];
        trackIndex = new int[this.trackManager.getTRACK_SIZE()];
    }

    /**
     * @param _trackNumber used to specify which track's playing status you want. cannot be bigger than index in trackmanager
     * @return true if the sound is playing or if the index asked is superior to the maximum index.
     */
    public boolean isPlaying(int _trackNumber) {
        return _trackNumber >= trackManager.getIndex() || playing[_trackNumber];
    }

    /**
     * Starts playing the soundtrack, loops automatically until stopSoundTrack() is called
     */
    public void playSoundtrack(final int _trackNumber) {
        if(_trackNumber < trackManager.getIndex()) {
            clock[_trackNumber] = new Timer();
            final SoundTrack soundtrack = trackManager.getTrack(_trackNumber);
            if (soundtrack.getSoundTrack().size() != 0) {
                for (int i = 0; i < soundtrack.getSoundTrack().size(); i++) {
                    TimedSound tempSound = (TimedSound) soundtrack.getSoundTrack().get(i);
                    long delay = tempSound.getTime();
                    clock[_trackNumber].schedule(new TimerTask() {
                        @Override
                        public synchronized void run() {
                            TimedSound ss = (TimedSound) soundtrack.getSoundTrack().get(trackIndex[_trackNumber]);
                            soundtrack.getPlayer().playSound(ss);
                            trackIndex[_trackNumber]++;
                        }
                    }, delay);
                }
                trackIndex[_trackNumber] = 0;
                clock[_trackNumber].schedule(new TimerTask() {
                    @Override
                    public void run() {
                        playSoundtrack(_trackNumber);
                    }
                }, soundtrack.getEndTime());

                playing[_trackNumber] = true;
            }
        }
    }

    /**
     * Stop the soundtrack instantly when it is called.
     */
    public void stopSoundtrack(final int _trackNumber) {
        if(_trackNumber < trackManager.getIndex()) {
            clock[_trackNumber].cancel();
            trackIndex[_trackNumber] = 0;
            playing[_trackNumber] = false;
        }
    }

}

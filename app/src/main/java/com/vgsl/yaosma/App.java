package com.vgsl.yaosma;

import android.app.Application;

/**
 * Created by victor on 19/02/15.
 */

public class App extends Application {

    private boolean editMode;

    public boolean getEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }
}
package com.vgsl.yaosma.model;

import com.vgsl.yaosma.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Giyomm on 09/04/2015.
 */
public class DefaultSoundList {

    public static ArrayList<Sound> list =
    new ArrayList<>(Arrays.asList(
        new Sound(R.raw.hat01, "hat01"),
        new Sound(R.raw.hat02, "hat02"),
        new Sound(R.raw.hat03, "hat03"),
        new Sound(R.raw.kick01, "kick01"),
        new Sound(R.raw.kick02, "kick02"),
        new Sound(R.raw.op_hat, "op_hat"),
        new Sound(R.raw.snr01, "snr01"),
        new Sound(R.raw.snr02, "snr02"),
        new Sound(R.raw.snr03, "snr03"),
        new Sound(R.raw.snr04, "snr04"),
        new Sound(R.raw.tom01, "tom01"),
        new Sound(R.raw.tom02, "tom02")));
}

package com.vgsl.yaosma.model;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.vgsl.yaosma.activity.MainActivity;
import com.vgsl.yaosma.listener.SoundButtonListener;

/**
 * Created by Giyomm on 12/02/2015.
 */
public class SoundButton extends Button {

    private Sound sound;

    public SoundButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void attachListener(MainActivity activity) {
        this.setOnTouchListener(new SoundButtonListener(activity, sound));
    }

    public Sound getSound() {
        return this.sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

    @Override
    public String toString() {
        return "SoundButton{" +
                "soundRes=" + sound.getResId() +
                ", soundId=" + sound.getSoundId() +
                '}';
    }
}

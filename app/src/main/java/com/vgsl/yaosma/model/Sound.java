package com.vgsl.yaosma.model;

/**
 * Each instance must be independent and not interfere with others.
 * It will save the state of being played in loop.
 * So this property of independence must be preserved in order to not create problems,
 * if the user wants to play in loop twice the same sound (in different buttons or
 * with the soundtrack for example)
 */
public class Sound {

    private int soundId; // id in the pool, used by play()
    private int resId; // id in application resources
    private PlayStyle playStyle = PlayStyle.PUNCTUAL;
    private String name;
    /**
     * returned by SoundPool.play,
     * needed by loop playStyle
     */
    private int streamId = -1;

    public Sound(int resId, String name) {
        this.soundId = -1;
        this.resId = resId;
        this.name = name;
    }

    public Sound(Sound sound) {
        this.soundId = sound.soundId;
        this.resId = sound.resId;
        this.playStyle = sound.playStyle;
        this.name = sound.name;
        this.streamId = sound.streamId;
    }

    public int getSoundId(){
        return soundId;
    }

    public void setSoundId(int soundId){
        this.soundId = soundId;
    }

    public int getResId(){
        return resId;
    }

    public void setResId(int resId){
        this.resId = resId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlayStyle getPlayStyle() {
        return playStyle;
    }

    public void setPlayStyle(PlayStyle playStyle) {
        this.playStyle = playStyle;
    }

    public int getStreamId() {
        return streamId;
    }

    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }

    public boolean isBeingPlayed() {
        return streamId != -1;
    }
}

package com.vgsl.yaosma.model;

/**
 * Created by Sykefu on 12/03/2015.
 */
public class TimedSound extends Sound{
    private long time; /**< new value used to track flow of time for use in SoundTracks */

    public TimedSound(Sound s, long time) {
        super(s);
        this.time = time;
    }

    public void setTime(long _time){
        time = _time;
    }

    public long getTime(){
        return time;
    }
}

package com.vgsl.yaosma.model;

import com.vgsl.yaosma.R;

import java.security.InvalidParameterException;

/**
 * Created by Giyomm on 12/03/2015.
 */
public enum PlayStyle {

    PUNCTUAL ("punctual", R.drawable.punctual),
    HOLD ("hold", R.drawable.hold),
    LOOP ("loop", R.drawable.loop);

    private String styleName;
    private int styleIcon;

    public String getStyleName() {
        return styleName;
    }

    public int getStyleIcon() {
        return styleIcon;
    }

    PlayStyle(String styleName, int styleIcon) {
        this.styleName = styleName;
        this.styleIcon = styleIcon;
    }

    public static PlayStyle find(String playStyleName) {
        switch (playStyleName) {
            case "punctual":
                return PUNCTUAL;
            case "hold":
                return HOLD;
            case "loop":
                return LOOP;
        }
        throw new InvalidParameterException("PlayStyle.find(...) called with invalid arguments");
    }
}

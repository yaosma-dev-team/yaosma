package com.vgsl.yaosma.model;

import com.vgsl.yaosma.audio.Player;
import com.vgsl.yaosma.model.SoundButton;
import com.vgsl.yaosma.model.TimedSound;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sykefu on 12/03/2015.
 */
public class  SoundTrack {

    private ArrayList<TimedSound> soundtrack;
    private long endTime;
    private Player player;
    public static final long MILLION = 1000000;

    /**
     *  builds a soundtrack.
     * @param _size size of the arraylist containing sounds
     * @param _player use a player which already loaded sounds to prevent multiple loading
     */

    public SoundTrack(int _size, Player _player){
        soundtrack = new ArrayList<TimedSound>(_size);
        player = _player;
    }

    public Player getPlayer() {
        return player;
    }

    public ArrayList getSoundTrack(){
        return soundtrack;
    }


    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

}


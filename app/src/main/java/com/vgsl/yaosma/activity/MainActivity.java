package com.vgsl.yaosma.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.ToggleButton;

import com.vgsl.yaosma.R;
import com.vgsl.yaosma.audio.Player;
import com.vgsl.yaosma.model.SoundTrack;
import com.vgsl.yaosma.audio.SoundTrackLoader;
import com.vgsl.yaosma.audio.SoundTrackManager;
import com.vgsl.yaosma.audio.SoundTrackPlayer;
import com.vgsl.yaosma.listener.EditSwitchListener;
import com.vgsl.yaosma.listener.SoundTrackButtonListener;
import com.vgsl.yaosma.model.DefaultSoundList;
import com.vgsl.yaosma.model.Sound;
import com.vgsl.yaosma.model.SoundButton;
import com.vgsl.yaosma.model.PlayStyle;

import java.util.ArrayList;

public class MainActivity extends Activity {

    public final static String EXTRA_BUTTON_NAME = "com.vgsl.yaosma.BUTTON_NAME";
    public final static String EXTRA_BUTTON_PLAYSTYLE = "com.vgsl.yaosma.BUTTON_PLAYSTYLE";
    public final static String EXTRA_BUTTON_SOUND_RES = "com.vgsl.yaosma.BUTTON_SOUND_RES";

    public final static int EDIT_BUTTON_REQUEST = 100;
    public SoundTrackManager trackManager;
    public SoundTrackLoader trackLoader;
    public SoundTrackPlayer trackPlayer;

    //TODO try to change with intent for button settings
    public int pressed_button_id;
    public Player player;

    private TableLayout mKeyboardTable;
    private Switch mModeSwitch;
    public ToggleButton mRecordSoundTrackButton;
    public LinearLayout mSoundTrackLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        player = new Player();
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        trackManager = new SoundTrackManager();
        trackLoader = new SoundTrackLoader();
        trackPlayer = new SoundTrackPlayer(trackManager);

        mKeyboardTable = (TableLayout)this.findViewById(R.id.table_layout_keyboard_id);
        mModeSwitch = (Switch) this.findViewById(R.id.switch_play_edit_mode_id);
        mModeSwitch.setOnCheckedChangeListener(new EditSwitchListener(this));
        mRecordSoundTrackButton = (ToggleButton) findViewById(R.id.toggle_Button_rec_id);
        mSoundTrackLayout = (LinearLayout) findViewById(R.id.horizontal_soundtrack_layout_id);

        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        init_button_matrix();
    }

    private void init_button_matrix() {
        ArrayList<Sound> defaultSoundList = DefaultSoundList.list;
        for(int row=0; row<mKeyboardTable.getChildCount(); ++row) {
            TableRow tRow = (TableRow) mKeyboardTable.getChildAt(row);
            int numberOfCol = tRow.getChildCount();
            for (int col=0; col<numberOfCol; ++col) {
                Sound localSound = defaultSoundList.get(row*numberOfCol +col);
                final int soundId = player.pool.load(this.getApplicationContext(), localSound.getResId(), 1);
                localSound.setSoundId(soundId);
                SoundButton sb = (SoundButton) tRow.getChildAt(col);
                sb.setText(localSound.getName());
                sb.setSound(localSound);
                sb.attachListener(this);
                sb.setCompoundDrawablesWithIntrinsicBounds(0,0,0,sb.getSound().getPlayStyle().getStyleIcon());
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data) {
        if (requestCode == EDIT_BUTTON_REQUEST && resultCode == RESULT_OK) {

            String name = data.getStringExtra(EditButtonActivity.EXTRA_BUTTON_EDITED_NAME);
            PlayStyle playstyle = (PlayStyle) data.getSerializableExtra(EditButtonActivity.EXTRA_BUTTON_EDITED_PLAYSTYLE);
            int newSoundRes = data.getIntExtra(EditButtonActivity.EXTRA_BUTTON_EDITED_SOUND_RES, -1);
            SoundButton soundButton = (SoundButton) this.findViewById(pressed_button_id);
            soundButton.setText(name);
            soundButton.getSound().setPlayStyle(playstyle);
            soundButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, soundButton.getSound().getPlayStyle().getStyleIcon());

            if (newSoundRes > 0) {
                storeNewResAndUpdatePool(newSoundRes, soundButton);
            }
        }
    }

    private void storeNewResAndUpdatePool(int newSoundRes, SoundButton soundButton) {
        soundButton.getSound().setResId(newSoundRes);
        int newSoundId = player.pool.load(this.getApplicationContext(), newSoundRes, 1);
        soundButton.getSound().setSoundId(newSoundId);
    }

    public void recordTrack(View v){
        if(mRecordSoundTrackButton.isChecked()){
            trackLoader.buildTrack(player);
        }
        else{
            trackManager.receiveTrack(trackLoader.sendTrack());
            if(trackManager.getLastTrack().getSoundTrack().size() != 0) {
                this.addSoundtrackButton(trackManager.getLastTrack());
                trackManager.incrementIndex();
                trackPlayer.playSoundtrack(trackManager.getIndex() - 1);
                if (!trackManager.sizeIsValid()) {
                    mRecordSoundTrackButton.setEnabled(false);
                }
            }
            else{
                trackPlayer.stopSoundtrack(trackManager.getIndex());
                trackManager.removeTrack(trackManager.getIndex());
                trackManager.incrementIndex();
                new AlertDialog.Builder(this)
                        .setTitle("Oops")
                        .setMessage("You must record at least one sound to create a soundtrack.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

    public void addSoundtrackButton(SoundTrack _st){
        Button soundTrackButton = new Button(this.getApplicationContext());
        soundTrackButton.setText("" + (trackManager.getIndex() + 1));
        mSoundTrackLayout.addView(soundTrackButton);
        soundTrackButton.setOnTouchListener(new SoundTrackButtonListener(this, trackPlayer, trackManager.getIndex(), trackManager));
    }

}

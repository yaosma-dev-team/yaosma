package com.vgsl.yaosma.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.vgsl.yaosma.R;
import com.vgsl.yaosma.audio.SoundMapper;
import com.vgsl.yaosma.listener.SoundListClickListener;
import com.vgsl.yaosma.model.PlayStyle;

import java.lang.reflect.Field;
import java.util.List;


public class EditButtonActivity extends ActionBarActivity {

    public final static String EXTRA_BUTTON_EDITED_NAME = "com.vgsl.yaosma.BUTTON_EDITED_NAME";
    public final static String EXTRA_BUTTON_EDITED_PLAYSTYLE = "com.vgsl.yaosma.BUTTON_EDITED_PLAYSTYLE";
    public final static String EXTRA_BUTTON_EDITED_SOUND_RES = "com.vgsl.yaosma.EXTRA_BUTTON_EDITED_SOUND_RES";

    private EditText mButtonNameEditText;
    private RadioGroup mRadioGroupPlayStyle;
    private int newRessourceSound;

    public void setNewRessourceSound(int soundResId){
        this.newRessourceSound=soundResId;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_button);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_BUTTON_NAME);
        PlayStyle playStyle = (PlayStyle) intent.getSerializableExtra(MainActivity.EXTRA_BUTTON_PLAYSTYLE);
        int soundResId = intent.getIntExtra(MainActivity.EXTRA_BUTTON_SOUND_RES, -1);

        mButtonNameEditText = (EditText) this.findViewById(R.id.edit_text_button_name_id);
        mButtonNameEditText.setText(message);

        mRadioGroupPlayStyle = (RadioGroup) this.findViewById(R.id.radio_group_playstyle_id);

        initRadioGroup(playStyle);

        displaySoundsList(soundResId);
    }

    private void initRadioGroup(PlayStyle playStyle) {
        for(int i = 0; i < mRadioGroupPlayStyle.getChildCount(); i++){
            RadioButton rb = (RadioButton) mRadioGroupPlayStyle.getChildAt(i);
            if(rb.getTag().equals(playStyle.getStyleName())) {
                rb.setChecked(true);
            }
        }
    }

    private void displaySoundsList(int currentSoundResId) {
        // TODO use currentSoundResId to highlight current sound in list
        Field[] sounds = R.raw.class.getFields();

        List<String> soundNames = SoundMapper.extract(sounds);
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, soundNames );
        ListView listView = (ListView) findViewById(R.id.list_view_sounds_id);
        listView.setAdapter(itemsAdapter);
        listView.setOnItemClickListener(new SoundListClickListener(this));
    }

    public void endEdit(View v) {
        switch (v.getId()) {
            case R.id.button_cancel_id:
                this.finish();

            case R.id.button_accept_id:
                // TODO get a PlayStyle instead of a String
                String newStyleName = this.findViewById(mRadioGroupPlayStyle.getCheckedRadioButtonId()).getTag().toString();
                PlayStyle selectedPlayStyle = PlayStyle.find(newStyleName);

                Intent backIntent = new Intent();
                backIntent.putExtra(EXTRA_BUTTON_EDITED_NAME, mButtonNameEditText.getText().toString());
                backIntent.putExtra(EXTRA_BUTTON_EDITED_PLAYSTYLE, selectedPlayStyle);
                backIntent.putExtra(EXTRA_BUTTON_EDITED_SOUND_RES, newRessourceSound);
                setResult(RESULT_OK, backIntent);
                this.finish();
        }
    }
}

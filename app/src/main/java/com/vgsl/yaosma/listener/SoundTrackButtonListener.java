package com.vgsl.yaosma.listener;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.vgsl.yaosma.App;
import com.vgsl.yaosma.activity.MainActivity;
import com.vgsl.yaosma.audio.SoundTrackManager;
import com.vgsl.yaosma.audio.SoundTrackPlayer;

/**
 * Created by sykefu on 26/03/15.
 */
public class SoundTrackButtonListener implements View.OnTouchListener{

    MainActivity mainActivity;
    private SoundTrackPlayer trackPlayer;
    private int trackIndex;
    private SoundTrackManager soundTrackManager;

    public SoundTrackButtonListener(MainActivity mainActivity, SoundTrackPlayer _trackPlayer, int _trackIndex, SoundTrackManager soundTrackManager) {
        this.mainActivity = mainActivity;
        this.trackPlayer = _trackPlayer;
        this.trackIndex = _trackIndex;
        this.soundTrackManager = soundTrackManager;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(inPlayMode() && buttonIsPressed(event)) {
            if(trackPlayer.isPlaying(trackIndex)) {
                trackPlayer.stopSoundtrack(trackIndex);
            }
            else if(!(trackPlayer.isPlaying(trackIndex))) {
                trackPlayer.playSoundtrack(trackIndex);
            }
        }
        else if(buttonIsPressed(event)){
            final Button tmpButton = (Button) v;
            new AlertDialog.Builder(mainActivity)
                    .setTitle("Hey!")
                    .setMessage("Are you sure you want to remove this soundtrack?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            int trackIndex = Integer.parseInt(tmpButton.getText().toString()) - 1;
                            trackPlayer.stopSoundtrack(trackIndex);
                            soundTrackManager.removeTrack(trackIndex);
                            mainActivity.mSoundTrackLayout.removeView(tmpButton);
                            for( trackIndex = 0; trackIndex < mainActivity.mSoundTrackLayout.getChildCount(); trackIndex++){
                                Button btn = (Button) mainActivity.mSoundTrackLayout.getChildAt(trackIndex);
                                btn.setText("" + (trackIndex + 1));
                                //TODO check if what is done here is not retarded
                                //en gros: je change les handlers juste pour changer le parametre i ici pour redefinir les bonnes valeurs de track a jouer
                                //pour comprendre la raison si jamais, commenter la ligne en dessous, creer 2 soundtrack dans l'appli, supprimer la premiere, jouer celle qui reste (ne fait rien au lieu de jouer la soundtrack sauvée en 2eme slot)
                                btn.setOnTouchListener(new SoundTrackButtonListener(mainActivity, trackPlayer, trackIndex, soundTrackManager));
                            }
                            if(soundTrackManager.sizeIsValid()){
                                mainActivity.mRecordSoundTrackButton.setEnabled(true);
                            }
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_delete)
                    .show();
        }
        return false;
    }

    private boolean inPlayMode() {
        boolean editMode = ((App) mainActivity.getApplication()).getEditMode();
        return !editMode;
    }

    private boolean buttonIsPressed(MotionEvent event) {
        return event.getAction() == MotionEvent.ACTION_DOWN;
    }

}

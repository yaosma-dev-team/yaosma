package com.vgsl.yaosma.listener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.vgsl.yaosma.activity.EditButtonActivity;
import com.vgsl.yaosma.R;
import com.vgsl.yaosma.audio.SoundMapper;

/**
 * Created by victor on 12/03/15.
 */
public class SoundListClickListener implements AdapterView.OnItemClickListener {
    private final EditButtonActivity editButtonActivity;

    public SoundListClickListener(EditButtonActivity editButtonActivity) {
        this.editButtonActivity = editButtonActivity;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedSoundName = (String) parent.getItemAtPosition(position);
        EditText soundNameField = (EditText) editButtonActivity.findViewById(R.id.edit_text_button_name_id);
        soundNameField.setText(selectedSoundName, TextView.BufferType.EDITABLE);
        editButtonActivity.setNewRessourceSound(SoundMapper.getResIdFromName(selectedSoundName));

    }
}

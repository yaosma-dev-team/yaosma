package com.vgsl.yaosma.listener;

import android.widget.CompoundButton;

import com.vgsl.yaosma.App;
import com.vgsl.yaosma.activity.MainActivity;

public class EditSwitchListener implements CompoundButton.OnCheckedChangeListener {
    private MainActivity mainActivity;


    public EditSwitchListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            ((App) mainActivity.getApplication()).setEditMode(true);
        } else {
            ((App) mainActivity.getApplication()).setEditMode(false);
        }
    }
}

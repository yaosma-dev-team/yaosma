package com.vgsl.yaosma.listener;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;

import com.vgsl.yaosma.App;
import com.vgsl.yaosma.activity.EditButtonActivity;
import com.vgsl.yaosma.activity.MainActivity;
import com.vgsl.yaosma.model.PlayStyle;
import com.vgsl.yaosma.model.Sound;
import com.vgsl.yaosma.model.SoundButton;

public class SoundButtonListener implements View.OnTouchListener {

    MainActivity mainActivity;
    private Sound sound;

    public SoundButtonListener(MainActivity mainActivity, Sound sound) {
        this.mainActivity = mainActivity;
        this.sound = sound;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(buttonIsPressed(event)) {
            onPress(v);
        }

        if(releasedAndHoldPlayStyle(event)) {
            mainActivity.player.pool.stop(sound.getStreamId());
            mainActivity.trackLoader.addReleased((SoundButton)v);
        }
        return false;
    }

    private boolean releasedAndHoldPlayStyle(MotionEvent event) {
        return buttonIsReleased(event) && inPlayMode() && sound.getPlayStyle() == PlayStyle.HOLD;
    }

    private void onPress(View v) {
        if(inPlayMode()) {
            mainActivity.player.playSound(sound);
            if(inTrackMode())
                mainActivity.trackLoader.addPressed((SoundButton)v);
        }
        else if(!inPlayMode()) {
            loadEditButtonActivity((SoundButton)v);
        }
    }

    public void loadEditButtonActivity(SoundButton v) {
        Intent intent = new Intent(mainActivity, EditButtonActivity.class);
        String name = v.getText().toString();
        int soundResId = v.getSound().getResId();
        PlayStyle playStyle = v.getSound().getPlayStyle();
        intent.putExtra(mainActivity.EXTRA_BUTTON_NAME, name);
        intent.putExtra(mainActivity.EXTRA_BUTTON_PLAYSTYLE, playStyle);
        intent.putExtra(mainActivity.EXTRA_BUTTON_SOUND_RES, soundResId);
        mainActivity.pressed_button_id = v.getId();
        mainActivity.startActivityForResult(intent,mainActivity.EDIT_BUTTON_REQUEST);
    }

    private boolean inPlayMode() {
        boolean editMode = ((App) mainActivity.getApplication()).getEditMode();
        return !editMode;
    }

    private boolean inTrackMode() {
        if(mainActivity.trackManager.sizeIsValid())
            return mainActivity.trackLoader.isRecording();
        else
            return false;
    }

    private boolean buttonIsPressed(MotionEvent event) {
        return event.getAction() == MotionEvent.ACTION_DOWN;
    }

    private boolean buttonIsReleased(MotionEvent event) {
        return event.getAction() == MotionEvent.ACTION_UP;
    }
}

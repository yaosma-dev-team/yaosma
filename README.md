YAOSMA: Yet Another Open Source (Music|Mix) App
====================================

Launchpad like application for Android.

### What does it do?
Play embedded sound when pressing a button.

Repeat a beat.

Add custom sounds

### What does it could eventually do in the future?

Save to own format.

Export to ogg, opus, mp3.

Manage layouts.


### External ressources
Audio samples from http://www.musicradar.com/news/tech/free-music-samples-download-loops-hits-and-multis-217833/65

License unknown but it seems to be not free/libre